Tasks = new Mongo.Collection("tasks");

if (Meteor.isClient) {

  Template.home.rendered = function () {
   console.log("in rendering");
    $('.send').show();
    $('.request').hide();
    $('.add').hide();
    $('.send-li').css('color','#ee6e73');
  }

  Template.body.events({
    "click body" : function (event) {
      console.log("in body");
      event.preventDefault();
    }
  });

  Template.home.events({
    "click .tabs .tab a": function (event) {
      event.preventDefault();
      console.log(event.target.text);
      var input = event.target.text;
      if (input === "Send" || input === "SEND") {
        $('.send').show();
        $('.request').hide();
        $('.add').hide();
        $('.send-li').css('color','#ee6e73');
      } else if (input === "Request" || input === "REQUEST") {
        $('.send').hide();
        $('.request').show();
        $('.add').hide();
        $('.send-li').css('color','#26A69A');
      } else if (input === "Add" || input === "ADD") {
        $('.send').hide();
        $('.request').hide();
        $('.add').show();
        $('.send-li').css('color','#26A69A');
      }
      // $(event.target).removeClass('active');
    }
  });

  // At the bottom of the client code
  // Accounts.ui.config({
  //   passwordSignupFields: "USERNAME_ONLY"
  // });
}

if (Meteor.isServer) {
  // Meteor.startup(function () {
  //   // code to run on server at startup
  // });
}
